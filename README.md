### What is this repository for? ###

* Password encryption for Tomcat Datasources. This depends on WINE to work on Linux. Works natively with 
   Windows. This is based on a blog post that I saw, will include the link when I can find it. 
* 0.1-POC
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Import POM project into your IDE of choice (Netbeans, Eclipse, IntelliJ)
* All dependencies should be pulled from the POM by Maven. 
* No tests yet. 
* Place .jar file in Lib directory of your container.